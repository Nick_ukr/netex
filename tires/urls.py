from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

from . import views

app_name = 'tires'
urlpatterns = [
    url(r'^$', views.TireListView.as_view(), name='list'),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.TireDeleteView.as_view(), name='delete'),
    url(r'^create/$', views.TireCreate.as_view(), name='create'),
    url(r'^edit/(?P<pk>[0-9]+)/$', views.TireUpdate.as_view(), name='edit')
]

urlpatterns += static(settings.MEDIA_URL, doccument_root=settings.MEDIA_ROOT)

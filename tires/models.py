from django.db import models


class Tire(models.Model):
    width = models.IntegerField()
    height = models.IntegerField()
    diameter = models.IntegerField()
    speed_index = models.IntegerField()
    picture = models.ImageField(upload_to='picturegroup')

    def __str__(self):
        return str(self.pk)

from django.urls import reverse_lazy
from django.views import generic
from .models import Tire


class TireListView(generic.ListView):
    template_name = 'tires/tires_list.html'
    context_object_name = 'tires_list'

    def get_queryset(self):
        return Tire.objects.all()


class TireCreate(generic.CreateView):
    model = Tire
    fields = ['width', 'height', 'diameter', 'speed_index', 'picture']
    success_url = reverse_lazy('tires:list')


class TireUpdate(generic.UpdateView):
    model = Tire
    fields = ['width', 'height', 'diameter', 'speed_index', 'picture']
    success_url = reverse_lazy('tires:list')


class TireDeleteView(generic.DeleteView):
    model = Tire
    success_url = reverse_lazy('tires:list')

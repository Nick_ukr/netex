from django.contrib import admin
from .models import Tire

@admin.register(Tire)
class TireAdmin(admin.ModelAdmin):
    pass
